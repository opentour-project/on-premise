# OpenTour On Premise

This repository contains an example setup of OpenTour with the help of
Docker and Docker compose.

This assumes that it will be deployed in an internal network instead
of being exposed to the internet.

If you plan to expose this to the internet, you might need reverse
proxy solutions such as [Traefik].

## Using the setup

This guide assumes that you've installed Docker and Docker compose. If
not, please read [Get Docker] and [Install Docker Compose].

1. Create a new secret key.

   - You need to change the secret key by editing the
     `docker-compose.yml`.
   - Once opened, simply replace the `PLEASE_CHANGE_ME` to a new
     secret key.
   - To generate a new secret key, you can use the `pwgen` utility and
     run the following `pwgen 32 1`.

2. Launch OpenTour

   - You can launch OpenTour by simply running `docker-compose up -d`.
   - It may take a while but OpenTour should be up on port 80.
     - To access simply open a web browser and go to
       `http://<the ip address of the machine>`.

3. Create new superuser

    - Once OpenTour is launched, you can now create a new superuser to
      start creating quests and questlines.
    - To do this, simply run
      `docker-compose exec app ./manage.py createsuperuser`
      and follow the prompts.

4. Visit the administration panel.

    - Now that you have a new superuser account, you can start
      creating quests and questlines by visiting the admin panel.
    - To access simply open a web browser and go to
      `http://<the ip address of the machine>/admin/`.

[Traefik]: https://traefik.io/
[Get Docker]: https://docs.docker.com/get-docker/
[Install Docker Compose]: https://docs.docker.com/compose/install/
